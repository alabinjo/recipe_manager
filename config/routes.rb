Rails.application.routes.draw do
  
  
  resources :sessions
  
  #named routes
  get    "login"  => "sessions#new"
  post   "login"  => "sessions#create"
  get    "logout" => "sessions#destroy"
  
  
  resources :chefs
  
  #named routes 
  get "register" => "chefs#new"
  
  resources :recipes do
    member do 
      post 'like'
    end 
  end 
  
  
  get "home" => 'pages#home'
  
  root "pages#home"

end 