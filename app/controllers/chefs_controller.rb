class ChefsController < ApplicationController
  before_action :set_chef, only: [:edit, :update, :show]
  before_action :require_same_user, only: [:edit, :update]
  
  def index 
    @chefs = Chef.all.paginate(page: params[:page], per_page: 2)
  end 
  
  def show 
    @recipes = @chef.recipes.paginate(page: params[:page], per_page: 3)
  end 
  
  def new 
    @chef = Chef.new 
  end 
  
  def create 
    @chef = Chef.new(chef_params)
    if @chef.save 
      flash[:success] = "Your account has been created succesfully!"
      session[:chef_id] = @chef.id
      redirect_to recipes_path 
    else 
      flash.now[:error] = "Please try again"
      render :new
    end 
  end 
  
  def edit 
  
  end 
  
  def update
    if @chef.update(chef_params)
      flash[:success] = "Chef has been updated"
      redirect_to chef_path(@chef)
    else
      flash.now[:error] ="Error editing the form"
      render :edit 
    end 
  end 
  
  private 
    def chef_params
      params.require(:chef).permit(:chefname, :email, :password)
    end 
    
    def require_same_user
      if current_user != @chef
        flash[:danger] = "You can only edit your own profile"
        redirect_to root_path
      end 
    end 
    
    def set_chef 
      @chef = Chef.find(params[:id])
    end 
  
end
