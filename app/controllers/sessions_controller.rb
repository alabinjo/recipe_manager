class SessionsController < ApplicationController
  def new
    
  end 
  
  def create
    # check to see if email is in database and brin up that user
    chef = Chef.find_by_email(params[:email])
    if chef.valid? && chef.authenticate(params[:password])
      session[:chef_id] = chef.id 
      flash["success"] = "You are logged in"
      redirect_to recipes_path 
    else 
      flash.now[:error] = "Please try again"
      render :new
    end 
    # authenticate that user with the password provided 
  end 
  
  def destroy
    #set user session to nil
    session[:chef_id] = nil
    flash[:success] = "You have logged out"
    redirect_to root_path
    #redirect to home
  end 
  
end
