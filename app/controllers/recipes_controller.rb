class RecipesController < ApplicationController
  before_action :set_recipe, only: [:edit, :update, :show, :like ]
  before_action :require_user, except: [:show, :index]
  before_action :require_same_user, only: [:edit, :update ]
  
  def index 
    @recipes = Recipe.paginate(page: params[:page], per_page: 4) 
  end 
  
  def show
  
  end 
  
  def new 
    @recipe = Recipe.new
  end 
  
  def create
    @recipe = Recipe.new(recipe_params)
    @recipe.chef = current_user
    if @recipe.save
      flash[:success] = "Your recipe was created successfully"
      redirect_to recipes_path 
    else 
      flash.now[:error] = "Error with your form"
      render :new 
    end
    
  end 
  
  def edit 
  
  end 
  
  def update 
    if @recipe.update(recipe_params)
      flash[:success] = "Recipe has been successfully updated"
      redirect_to recipe_path(@recipe)
    else 
      flash.now[:error] = "You have an error with your form"
      render :edit
    end 
  end 
  
  def destroy 
  end 
  
  def like 
    @like = Like.new(like: params[:like], chef: current_user, recipe: @recipe)
    if @like.valid?
      if @like.save
        flash[:success] = "Your selection was successful"
        #user stays on the page where the like is made 
        redirect_to :back 
      else 
        flash[:"you can only like/dislike a recipe once"]
        redirect_to :back 
      end
    end 
    
  end
  
  private 
    def recipe_params 
      params.require(:recipe).permit(:name, :summary, :description, :id, :picture)
    end 
   
    def require_same_user
      if current_user != @recipe.chef
        flash[:danger] = "You can only edit your own profile"
        redirect_to recipes_path 
      end 
    end 
    
    def set_recipe
      @recipe = Recipe.find(params[:id])
    end 
  
end
