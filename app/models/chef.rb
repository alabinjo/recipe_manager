class Chef < ActiveRecord::Base
  #before any object is saved all emails with be downcased 
  before_save { self.email = email.downcase }
  has_many :recipes 
  has_many :likes
  validates :chefname, presence: true, length: { in: 3..40 }
  VALID_EMAIL_REGEX = /\A[\w+\-.]+@[a-z\d\-]+(\.[a-z]+)*\.[a-z]+\z/i
  validates :email, presence: true, length: { in: 1..105 }, uniqueness: { case_sensitive: false },
                                                         format: { with: VALID_EMAIL_REGEX }
                                                         
  has_secure_password

end
