require 'test_helper'

class RecipeTest < ActiveSupport::TestCase
 
 
#create a receipe object (nanme, summary, and description)
#save it to an instance variable so it can be used for all test cases 
#need to setup 

  def setup
    @chef = Chef.create(chefname: "Ade", email: "adelabinjo@gmail.com")
    @recipe = @chef.recipes.build(name: "chicken broth", summary: "this is th best chicken broth ever",
                            description: "heart all, add onions, add tomato sauce, and chicken" )
  end 
   
  test "recipe should be valid" do 
     assert @recipe.valid?
  end 
  
  test "chef_id should be valid" do
    @recipe.chef_id = nil 
    assert_not @recipe.valid?
  end 
  
  test "name should be present" do 
    
    @recipe.name = " "
    assert_not @recipe.valid?
    
  end
  
  test "name length should not be too long" do 
    @recipe.name = "a" * 101
    assert_not @recipe.valid?
  end 
   
  test "name length should not be too short" do 
    @recipe.name = "a" * 2 
    assert_not @recipe.valid? 
  end 
  
  test "summary should be present" do 
    @recipe.summary = " "
    assert_not @recipe.valid?
  end 
  
  test "summary length should not be too long" do
    @recipe.summary = "a" * 151
    assert_not @recipe.valid?
  end 
  
  test "summary length should not be too short" do
    @recipe.summary = "a" * 9
    assert_not @recipe.valid?
  end 
  
  test "description must be present" do 
    @recipe.description = " "
    assert_not @recipe.valid?
  end 
  
  test "description should not be too long" do 
    @recipe.description = "a" * 501
    assert_not @recipe.valid?
    
  end 
  
  test "description should be too short" do
    @recipe.description = "a" * 19
    assert_not @recipe.valid?
  end 
  
  
end
